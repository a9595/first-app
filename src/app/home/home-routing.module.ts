import { NgModule } from '@angular/core'
import { RouterModule, Routes } from '@angular/router'
import { MainPageComponent } from './main-page/main-page.component'
import { RickAndMortyoute } from './routes/rick-and-morty'
import { LayoutComponent } from './components/layout/layout.component'
import { LifecycleHooksComponent } from './components/lifecycle-hooks/lifecycle-hooks.component'

const routes: Routes = [
  {
    path: '',
    component: LayoutComponent,
    children: [
      RickAndMortyoute,
      {
        path: 'lifecycle-hooks',
        component: LifecycleHooksComponent
      },
      {
        path: 'main-page',
        component: MainPageComponent
      },
      {
        path: '',
        redirectTo: 'main-page',
        pathMatch: 'full'
      }
    ]
  }
]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomeRoutingModule {}
