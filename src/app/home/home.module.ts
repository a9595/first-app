import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'

import { HomeRoutingModule } from './home-routing.module'
import { MainPageComponent } from './main-page/main-page.component'
import { LayoutComponent } from './components/layout/layout.component'
import { LifecycleHooksComponent } from './components/lifecycle-hooks/lifecycle-hooks.component'
import { SpyDirective } from './directives/spy.directive'

@NgModule({
  declarations: [
    MainPageComponent,
    LayoutComponent,
    LifecycleHooksComponent,
    SpyDirective
  ],
  imports: [CommonModule, HomeRoutingModule]
})
export class HomeModule {}
