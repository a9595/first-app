import { Component } from '@angular/core'
import { BehaviorSubject, Observable } from 'rxjs'
import { LoggerServiceService } from 'src/app/services/logger-service.service'
import { ITask } from '../../models/task.model'

@Component({
  selector: 'app-lifecycle-hooks',
  templateUrl: './lifecycle-hooks.component.html',
  styleUrls: ['./lifecycle-hooks.component.scss']
})
export class LifecycleHooksComponent {
  private _tasks: BehaviorSubject<ITask[]> = new BehaviorSubject<ITask[]>([])
  tasks$: Observable<ITask[]>

  constructor(private _looger: LoggerServiceService) {
    this.tasks$ = this._tasks.asObservable()
  }

  // ngOnInit(): void {
  //   this._looger.log('LifecycleHooksComponent ngOnInit')
  // }

  addBtnHandler(): void {
    const currenTask = this._tasks.getValue()
    const task: ITask = {
      guid: currenTask.length + 1,
      description: 'Hola',
      order: 1,
      favorite: true,
      owner: 'string'
    }

    currenTask.push(task)
    this._tasks.next(currenTask)
  }

  removeBtnHandler(): void {
    const currenTask = this._tasks.getValue()

    currenTask.splice(1, 1)
    this._tasks.next(currenTask)
  }
}
