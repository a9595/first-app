import { Route } from '@angular/router'

export const RickAndMortyoute: Route = {
  path: 'rick-and-morty',
  loadChildren: () =>
    import('../../rick-and-morty/rick-and-morty.module').then(
      (m) => m.RickAndMortyModule
    )
}
