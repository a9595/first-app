export interface ITask {
  guid: number
  description: string
  order: number
  favorite: boolean
  owner: string
}
