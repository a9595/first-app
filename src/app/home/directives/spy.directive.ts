import { Directive, OnDestroy, OnInit } from '@angular/core'
import { LoggerServiceService } from 'src/app/services/logger-service.service'

let nextId = 1
@Directive({
  selector: '[appSpy]'
})
export class SpyDirective implements OnInit, OnDestroy {
  private id = nextId++

  constructor(private logger: LoggerServiceService) {}

  ngOnInit() {
    this.logger.log(`Spy #${this.id} onInit`)
  }

  ngOnDestroy() {
    this.logger.log(`Spy #${this.id} onDestroy`)
  }
}
