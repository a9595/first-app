import { Route } from '@angular/router'

export const AuthRoute: Route = {
  path: 'auth',
  loadChildren: () => import('../auth/auth.module').then((m) => m.AuthModule)
}
