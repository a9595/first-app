import { Route } from '@angular/router'

export const HomeRoute: Route = {
  path: 'home',
  loadChildren: () => import('../home/home.module').then((m) => m.HomeModule)
}
